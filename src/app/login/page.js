'use client';
import Image from 'next/image';
import { Container, Row, Col, Card, Form, FloatingLabel, Button } from 'react-bootstrap';
import btnBack from "./../../../public/assets/arrow-left.svg";

export default function Login() {
	return (
		<main>
			<Container fluid className='form-login'>
				<Row className='justify-content-start vh-100'>
					<Col sm={3} md={3}>
						<Button variant="light" size="lg" className='rounded-1 border-0 font-16 mt-5 position-absolute btn-back'>
							<Image
								src={btnBack}
								className='img-logo img-fluid'
								width={25}
							/>
						</Button>
					</Col>
					<Col sm={6} md={6}>
						<Card className='border-0 my-5'>
							<Card.Body className='p-5'>
								<h5>Login</h5>
								<Form className='mt-4'>
									<FloatingLabel
										controlId="floatingInput"
										label="Email address"
										className="mb-3"
									>
										<Form.Control type="email" placeholder="name@example.com" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingPassword" label="Password">
										<Form.Control type="password" placeholder="Password" className='rounded-1' />
									</FloatingLabel>
									<a href='#' className="text-muted text-decoration-underline font-14 mt-2">
										Forgot Password?
									</a>
								</Form>
								<div className="d-grid gap-2 mt-5r">
									<Button variant="secondary" size="lg" className='rounded-1 border-0 font-16'>
										Login
									</Button>
								</div>
								<p class="text-center font-14 mt-2 mb-0">Don’t have an account? <a href='#' className="text-decoration-underline">
									Register here
								</a></p>
							</Card.Body>
						</Card>
					</Col>
					<div className="footer-login"></div>
				</Row>
			</Container >
		</main>
	)
}

