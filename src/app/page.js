'use client';
import Image from 'next/image';
import { Button, Container, Navbar, Nav, Form, Row, Col } from 'react-bootstrap';
import logo from "./../../public/assets/logo.svg";

export default function Home() {
  return (
    <main>
      {/* NAVBAR */}
      <Navbar expand="lg" className="bg-transparent py-4" >
        <Container>
          <Navbar.Brand href="#" className='position-relative'>
            <Image
              src={logo}
              className='img-logo img-fluid'
              style={{ maxWidth: '120px' }}
            />
            <span className='font-12 position-absolute ms-2'>Container Insurance System</span>
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="navbarScroll" />
          <Navbar.Collapse id="navbarScroll">
            <Nav
              className="ms-auto my-3 my-lg-0 font-14 me-2"
              style={{ maxHeight: '150px' }}
              navbarScroll
            >
              <Nav.Link href="#action1">Tentang Consure</Nav.Link>
              <Nav.Link href="#action2">Benefit</Nav.Link>
              <Nav.Link href="#action3">Spesifikasi Produk</Nav.Link>
              <Nav.Link href="#action4">Cara Kerja</Nav.Link>
            </Nav>
            <Form className="d-flex">
              <Button variant="secondary" className='font-12 rounded-1 border-0 btn-login'>Login</Button>
            </Form>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      {/* HERO */}
      <Container fluid className='hero'>
        <Row className='justify-content-center px-2' style={{ minHeight: '600px' }}>
          <Col sm={8} md={8}>
            <div className='text-center text-light title-hero'>
              <h1>Container Insurance System</h1>
              <h4 className='fw-normal'>Affordable . Simple . Secure</h4>
              <Button variant="light" className='px-5 py-3 mt-5 text-white rounded-1 border-0'>TRY IT NOW</Button>
            </div>
          </Col>
        </Row>
      </Container>
    </main>
  );
}
