'use client';
import Image from 'next/image';
import { Container, Row, Col, Card, Form, FloatingLabel, Button } from 'react-bootstrap';
import btnBack from "./../../../public/assets/arrow-left.svg";

export default function Registration() {
	return (
		<main>
			<Container fluid className='form-login'>
				<Row className='justify-content-start'>
					<Col sm={3} md={3}>
						<Button variant="light" size="lg" className='rounded-1 border-0 font-16 mt-5 position-absolute btn-back'>
							<Image
								src={btnBack}
								className='img-logo img-fluid'
								width={25}
							/>
						</Button>
					</Col>
					<Col sm={6} md={6}>
						<Card className='border-0 my-5'>
							<Card.Body className='p-5'>
								<h5>Registration</h5>
								<Form className='mt-4'>
									<FloatingLabel controlId="floatingSelect" label="Account Type" className='mb-3'>
										<Form.Select aria-label="Floating label select example">
											<option>Choose Account</option>
											<option value="1">One</option>
											<option value="2">Two</option>
											<option value="3">Three</option>
										</Form.Select>
									</FloatingLabel>
									<FloatingLabel
										controlId="floatingInput"
										label="Email address"
										className="mb-3"
									>
										<Form.Control type="email" placeholder="name@example.com" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="First Name" className="mb-3">
										<Form.Control type="text" placeholder="First Name" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Last Name" className="mb-3">
										<Form.Control type="text" placeholder="Last Name" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Password" className="mb-3">
										<Form.Control type="password" placeholder="Password" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingSelect" label="Country Code" className='mb-3'>
										<Form.Select aria-label="Floating label select example">
											<option>Choose Code</option>
											<option value="1">One</option>
											<option value="2">Two</option>
											<option value="3">Three</option>
										</Form.Select>
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Phone Number" className="mb-3">
										<Form.Control type="text" placeholder="Phone Number" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Company Name" className="mb-3">
										<Form.Control type="text" placeholder="Company Name" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Address" className="mb-3">
										<Form.Control type="text" placeholder="Address" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="City" className="mb-3">
										<Form.Control type="text" placeholder="City" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Zip Code" className="mb-3">
										<Form.Control type="text" placeholder="Zip Code" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Email" className="mb-3">
										<Form.Control type="email" placeholder="Email" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Create Password" className="mb-3">
										<Form.Control type="password" placeholder="Create Password" className='rounded-1' />
									</FloatingLabel>
									<FloatingLabel controlId="floatingInput" label="Confirm Password" className="mb-3">
										<Form.Control type="password" placeholder="Confirm Password" className='rounded-1' />
									</FloatingLabel>
								</Form>
								<div className="d-grid gap-2">
									<Button variant="secondary" size="lg" className='rounded-1 border-0 font-16'>
										Submit
									</Button>
								</div>
								<p class="text-center font-14 mt-2 mb-0">Do you have an account? <a href='#' className="text-decoration-underline">
									Login
								</a></p>
							</Card.Body>
						</Card>
					</Col>
					<div className="footer-login"></div>
				</Row>
			</Container >
		</main>
	)
}

